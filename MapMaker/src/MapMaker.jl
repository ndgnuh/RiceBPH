module MapMaker

using ImageFiltering
using GLMakie

const ROOT_BG = RGBf(0.95f0, 0.95f0, 0.98f0)
function heatmap_ax_options()
    return (
        alignmode=Outside(),
        aspect=DataAspect(),
        xzoomlock=true,
        yzoomlock=true,
        xpanlock=true,
        ypanlock=true,
        xrectzoom=false,
        yrectzoom=false,
    )
end

function textbox_options()
    return (
        halign=:left,
        width=Relative(1),
        alignmode=Outside()
    )
end

function main()
    @info "Creating figure"
    fig = Figure(backgroundcolor=ROOT_BG)

    # Input panel
    # ===========
    @info "Creating input panel"
    input_panel = fig[1, 1] = GridLayout(alignmode=Outside())


    # Map size
    Label(input_panel[1, 1], "Map size", halign=:left)
    map_size_tb = Textbox(input_panel[1, 2]; placeholder="Positive integer only", textbox_options()...)
    map_size = lift(map_size_tb.stored_string) do s
        something(tryparse(Int, something(s, "")), 100)
    end

    # Output file
    idx = input_panel.size[1] + 1
    Label(input_panel[idx, 1], "Path to output file", halign=:left)
    output_tb = Textbox(input_panel[idx, 2]; textbox_options()...)

    # Action buttons
    idx = input_panel.size[1] + 1
    ok_button = Button(input_panel[idx, 1], label="Ok")
    Button(input_panel[idx, 2], label="Save map")
    on(ok_button.clicks) do (_)
        map_size_tb.stored_string[] = map_size_tb.displayed_string[]
    end

    # Output panel
    # ============
    @info "Creating visualization panel"
    output_panel = fig[1:2, 2] = GridLayout()
    colgap!(output_panel, 0)

    # Food map
    food_map = @lift begin
        @show map_size
        rand(Float32, $map_size[], $map_size[])
    end
    food_map_ax = Axis(output_panel[1, 1]; title="Food map", heatmap_ax_options()...)
    hmap = heatmap!(food_map_ax, food_map; color=:green)
    Colorbar(output_panel[end, 2], hmap, colorrange=(0, 1))
    on(events(food_map_ax).mousebutton) do ev
        if ev.button == Mouse.left && ev.action == Mouse.press
            @show ev
        end
    end

    # Elimination map
    el_map = lift(food_map) do fm
        imfilter(fm, Kernel.gaussian(3))
    end
    el_map_ax = Axis(output_panel[end+1, 1]; title="Elimination map", heatmap_ax_options()...)
    hmap = heatmap!(el_map_ax, el_map; colormap=:reds)
    Colorbar(output_panel[end, 2], hmap)

    # Layout adjustment
    # =================
    colsize!(fig.layout, 1, Relative(0.3))
    colsize!(fig.layout, 2, Relative(0.7))
    colsize!(input_panel, 1, Relative(0.3))
    colsize!(input_panel, 2, Relative(0.7))
    on(map_size) do _
        autolimits!(food_map_ax)
        autolimits!(el_map_ax)
    end

    @info "Displaying everything"
    wait(display(fig))
end


if abspath(PROGRAM_FILE) == @__FILE__
    main()
end

end # module MapMaker
